## Nadia's Individual Growth Plan for Q3-Q4 (August 2020 - January 2021)


### 1. Become a stronger Design Leader by obtaiing new leadership skills
* [ ] Complete [UX Management: Strategy and Tactics training](https://www.interaction-design.org/courses/ux-management-strategy-and-tactics)  => 13%, 29/218 points
* [ ] Complete [GitLab Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/)  => 50%, 10/20 tasks
	
	
### 2. Undersatnd and leverage iteration thinking
* [ ] Complete [GitLab iteration training](https://gitlab.com/gitlab-com/Product/-/issues/1545)  => 0%, 0/20 tasks
* [ ] Provide evidence on how I facilitated braking big initiatives down into smaller steps for easier implementation  => 0%, 0/3 initiatives
* [ ] Guide teams to define MVC and guide them in breaking down the UX work  => 0%, 0/3 projects
	
	
### 3. Become a better professional coach to my team members
* [ ] Read ["Co-active Caching" by Henry Kimsey-House](https://www.amazon.com/Co-Active-Coaching-Fourth-transformative-conversations/dp/1473674980/)   => 0%, 0/12 chapters
* [ ] Complete [GitLab Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/)  => 50%, 10/20 tasks
	
	
### 4. Stay up to date with UX industry knowldege	
* [ ] Attend 2 Product Design / Design Leadership events  => 50%, 1/2 events
* [ ] Attend 2 DevOps events  => 0%, 0/2 events