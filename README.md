My name is Nadia Udalova - I am [UX Manager](https://about.gitlab.com/job-families/engineering/ux-management/) for [CI/CD](https://about.gitlab.com/product/continuous-integration/) at GitLab. 

This is my personal README - as a sort of "my user manual" for people who would like to learn about ways I like to work, collaborate and communicate.

- [GitLab Handle](https://gitlab.com/nudalova)
- [Team Page](https://about.gitlab.com/company/team/#nudalova)
- [GitLab Work Tracker](https://gitlab.com/nudalova/nadias-work-board/-/boards/1570805) 
- [My Website](https://www.nadiaudalova.com/)
- [Blog](https://medium.com/@nadiaudalova)

# About Me
- Born and raised in the west of Ukraine. I moved to the Netherlands at the age of 21, packing everything I owned (including the dog) and going to search for a better life
- Studied Business Management and Graphic Arts. This is one of the rare cases when education was useful and is related to the current job :)
- Worked in big companies and small - HewlettPackard, XebiaLabs
- Before joining GitLab I had 5 years of experience working in the DevOps domain
- Have a twin-sister (identical). It is the best thing ever!
- Enjoy chatting with people, organizing UX events, doing sports, cuddling dogs, eating healthy food :)

# How I work
- Conditions I like to work in: depends on the day! I like the quiet working environment when I need to focus, however, I love being surrounded by people too! 
- The times/hours I like to work: I am an early bird. I find mornings the most productive time
- The best ways to communicate with me: reach out directly in Slack or give me a call
- The ways I like to receive feedback: directly and face to face to be able to learn and improve
- Things I need: clear goals to be able to reach the North Star and lead others with me; authenticity - I find it hard to be around bullshit, passive-aggressiveness, inconsistency or incongruence 
- Things I struggle with: dishonesty, people being late to appointments
- Things I encourage: teamwork, having fun at work, setting yourself up for a greater challenge

# My leadership style: Facilitative
- I value the synergy of bringing together the different strengths of individuals and disciplines
- Working with others to make things easier and help to get things done
- Building understanding in a group, even when their views are in conflict
- Ensuring clarity of expectations, in terms of goals, roles, decisions
- Encouraging speaking up when there are challenges, self–critique, and improvement


# Things you should know about me
- I love travelling and seeing new places. Would be happy to meet many GitLab team-members in differents cities in coming future ;)
- If I were an animal I would be a dog, as a symbol of loyalty, protection, playfulness, determination, trustworthy and dependability
- Favorite quote: It is good to have an end to journey toward, but it is the journey that matters in the end. 



